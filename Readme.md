LibInsult Master Repo
=====================

This repository contains all the libinsult repositories as submodules.


License
-------

GPLv3+, see COPYING


Sources
-------

See http://insult.mattbas.org


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
