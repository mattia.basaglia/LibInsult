#
# Copyright (C) 2016-2022 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json
import os
from six.moves import urllib

import insult

from django.http import HttpResponse, HttpResponseBadRequest
from django.conf import settings
from django.template import loader
from django.views.generic import View
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt


class InsulterError(Exception):
    pass


def setting(name, default):
    return getattr(settings, "INSULT_"+name.upper(), default)


class FormatBase(View):
    insulter_dir = setting("WORD_PATH", None)
    default_language = setting("LANGUAGE", "en")
    default_template = None
    max_repetitions = setting("MAX_REPETITIONS", 5)
    allow_override_language = True
    allow_override_template = True

    def valid_language(self, lang):
        return all(c.isalpha() or c == "_" for c in lang)

    def insulter(self, lang):
        if not self.insulter_dir:
            ## \todo Provide this as a constant in libinsult
            self.insulter_dir = os.path.join(
                os.path.dirname(os.path.dirname(insult.__file__)),
                "share",
                "libinsult",
                "word_lists"
            )

        path = os.path.join(self.insulter_dir, lang)

        if not self.valid_language(lang) or not os.path.isdir(path):
            raise InsulterError("Invalid language")

        insulter = insult.Insulter()
        insulter.load_directory(path)
        insulter.set_max(self.max_repetitions)
        return insulter

    def _get_template(self, insulter, args):
        if self.allow_override_template and "template" in args:
            return args["template"]

        if self.default_template is not None:
            return self.default_template

        if "who" in args:
            if "plural" in args:
                return insulter.default_template(3, True, None).replace("<who>", args["who"])
            else:
                return insulter.default_template(3, False, None).replace("<who>", args["who"])
        else:
            return insulter.default_template(None, None, None)

    def _get_lang(self, args):
        return args.get("lang", self.default_language) if self.allow_override_language else self.default_language

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        response["Access-Control-Allow-Origin"] = "*"
        return response

    def get(self, request):
        return self.format_response(request, self.handle(request.GET))

    def format_response(self, request, response):
        response["X-Insult-Url"] = request.get_full_path()
        return response

    def handle(self, request_args):
        try:
            args = request_args.copy()
            lang = self._get_lang(args)
            args["lang"] = lang
            insulter = self.insulter(lang)
            template = self._get_template(insulter, request_args)
            args["template"] = template
            insult = self.format(insulter.format(template), args)
            return HttpResponse(insult, content_type=self.content_type)
        except InsulterError as err:
            return self.bad_request(self.error(str(err)))
        except Exception:
            if settings.DEBUG:
                raise
            return self.bad_request(self.error("Unexpected error"))

    def bad_request(self, contents):
        return HttpResponseBadRequest(contents, content_type=self.content_type)

    def error(self, error):
        return HttpResponseBadRequest()

    def format(self, insult, args):
        raise NotImplementedError()


class JsonFormat(FormatBase):
    content_type = "text/json"
    name = "json"

    def format(self, insult, args):
        return json.dumps({
            "error": False,
            "args": args,
            "insult": insult,
        })

    def error(self, error):
        return json.dumps({
            "error": True,
            "error_message": error
        })


class TextFormat(FormatBase):
    content_type = "text/plain"
    name = "txt"

    def format(self, insult, args):
        return insult + "\n"

    def error(self, error):
        return ""


class HtmlFormat(FormatBase):
    content_type = "text/html"
    name = "html"

    def render(self, template, **kwargs):
        return loader.get_template(template).render(kwargs)

    def format(self, insult, args):
        return self.render("django_insult/format.html", insult=insult)

    def error(self, error):
        return self.render(
            "django_insult/error.html",
            message=error,
            error_code=400
        )


output_formats = [TextFormat(), JsonFormat(), HtmlFormat()]


class FormatDispatcher(FormatBase):
    def get(self, request, format, **kwargs):
        params = request.GET.copy()
        params.update(kwargs)
        for format_object in output_formats:
            if format_object.name == format:
                self.format_object = format_object
                return self.format_response(request, self.handle(params))
        return self.format_response(request, HttpResponseNotFound())

    def format(self, insult, args):
        return self.format_object.format(insult, args)

    def error(self, insult):
        return self.format_object.error(insult)

    @property
    def content_type(self):
        return self.format_object.content_type

    @property
    def name(self):
        return self.format_object.name


def help(request):
    return render(request, "django_insult/api_help.html", {
        "formats": output_formats,
        "default_language": FormatBase.default_language,
        "default_template": FormatBase.default_template,
    })


def get_url(args):
    lang = args.pop("lang", None)
    format = args.pop("format", "txt")

    if "who" in args and not args["who"]:
        args.pop("who")

    if "template" in args and not args["template"]:
        args.pop("template")

    if lang:
        url = reverse("insult:api_lang", kwargs=({
            "lang": lang,
            "format": format
        }))
    else:
        url = reverse("insult:api", kwargs=({
            "format": format
        }))

    if args:
        url += "?" + urllib.parse.urlencode(args)

    return url


@csrf_exempt
def explore(request):
    if request.method == "POST":
        return redirect(get_url(request.POST.dict()))

    return render(request, "django_insult/api_explore.html", {
        "languages": [
            lang
            for lang in os.listdir(FormatBase.insulter_dir)
            if os.path.isdir(os.path.join(FormatBase.insulter_dir, lang))
        ],
        "default_language": FormatBase.default_language,
        "formats": output_formats,
    })
